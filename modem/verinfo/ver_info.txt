{
    "Image_Build_IDs": {
        "adsp": "ADSP.VT.5.4.2-00407-MANNAR-1", 
        "boot": "BOOT.XF.4.2-00290-MANNARLAZ-2", 
        "btfm": "BTFM.CHE.3.2.1-00272-QCACHROMZ-2", 
        "btfm_CHK": "BTFM.CHE.2.1.6-00075-QCACHROMZ-1", 
        "cdsp": "CDSP.VT.2.4.2-00318.1-MANNAR-1", 
        "common": "Strait.LA.2.1.2-00011-STD.PROD-1.52377.2", 
        "glue": "GLUE.STRAIT_LA.2.1.1-00005-NOOP_TEST-1", 
        "modem": "MPSS.HI.4.3.4-00562-MANNAR_GEN_PACK-2", 
        "rpm": "RPM.BF.1.11-00134-MANNARAAAAANAZR-1", 
        "tz": "TZ.XF.5.1-01171-MANNARAAAAANAZT-3.52377.4", 
        "tz_apps": "TZ.APPS.2.0-00273-MANNARAAAAANAZT-1", 
        "video": "VIDEO.VE.6.0-00052-PROD-6", 
        "wlan": "WLAN.HL.3.3.2-00501-QCAHLSWMTPLZ-1"
    }, 
    "Metabuild_Info": {
        "Meta_Build_ID": "Strait.LA.2.1.2-00011-STD.PROD-1.52377.2", 
        "Product_Flavor": "asic", 
        "Time_Stamp": "2024-08-06 16:05:58"
    }, 
    "Version": "1.0"
}